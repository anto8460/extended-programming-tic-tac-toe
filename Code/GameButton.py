# A game button that inherits from the tkinter class Button
# and add one more attribute called token
# which will be responsible for the game logic

from tkinter import Button

class GameButton(Button):

    def __init__(self, parent=None, token=None, *args, **kwargs):
        Button.__init__(self, parent, *args, **kwargs)
        self.token = token

    # Method to change the token of a button
    def changeToken(self, new_token):
        self.token = new_token

    



#import tkinter as tk

#mainwindow = tk.Tk()
#mybutton = GameButton(mainwindow, text="test", width=70, height=70)
#mybutton2 = GameButton(mainwindow, token="test", text="test", width=70, height=70)
#print(mybutton.token)
#mybutton2.changeToken("TEST2")
#print(mybutton2.token)

#mybutton.pack()
#print(mybutton)
#mainwindow.mainloop()