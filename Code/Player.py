# Class Player responsible for creating and using a
# player object that will be holding the score 
# and the name of the player

class Player:

    def __init__(self, Username, token, turn):
        self.user_name = Username
        self.token     = token
        self.turn      = turn
        self.score     = 0

    def setScore(self):
        self.score += 1
    
    def clearScore(self):
        self.score = 0

#player1 = Player("Toni", "test")
#player2 = Player("Boni", "test")

#for i in range (9):
#    player1.setScore()
#    player2.setScore()
#    print(player1.score)
#    print(player2.score)

#player1.clearScore()
#player2.clearScore()

#print(player1.score, player2.score)

