#   GameWindow - a custom class made to create 
#   the main window for the game.
#   Inherits from the tkinter class - Tk
#   GameWindow class is a Singleton!

import tkinter as tk

class GameWindow(tk.Tk):
   
    __instance = None

    @staticmethod
    def getInstance():
        if GameWindow.__instance == None:
            GameWindow()
        return GameWindow.__instance

    def __init__(self, *args, **kwards):
        if GameWindow.__instance != None:
                raise Exception("This class is a singleton!")
        else:
            GameWindow.__instance = self
            tk.Tk.__init__(self,  *args, **kwards)
    
