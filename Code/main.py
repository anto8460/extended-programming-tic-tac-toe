import GameApp
import GameWindow

if __name__ == "__main__":
    root = GameWindow.GameWindow()

    myApp = GameApp.GameApp(root)
    myApp.createImages()
    myApp.createLabels()
    myApp.createEntrys()
    myApp.createButtons()
    myApp.createStartFrame()
    myApp.createPlayFrame()

    root.mainloop()
