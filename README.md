# Extended Programming - Tic-Tac-Toe

###### Create a Multiplayer Tic-Tac-Toe game with Python

### Project Description 

Using programming patterns and object-oriented programming this project will be working on creating a Tic-Tac-Toe game with Python  using the open source libraries which provides features for graphical user interface. The Game will have two modes - Single player and Multiplayer. In single player the user will be playing against the computer and in multiplayer the user will be playing against another player (possibly from over the internet). The game will keep track of scores and top players.

