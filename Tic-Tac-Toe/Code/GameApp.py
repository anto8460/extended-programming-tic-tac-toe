import Player
import GameWindow
import tkinter      as tk
import tkinter.font as font
import FrameBuilder as FB
import GameButton   as GB
import GameLogic    as GL
from functools import partial
from PIL       import ImageTk, Image


class GameApp():
    
    def __init__(self, window):
        # Design main window
        self.window = window
        self.window.geometry("400x450")
        self.icon = tk.PhotoImage(file="icon.png")
        self.window.title("Tic-Tac-Toe")
        self.window.iconphoto(True, self.icon)
        self.backgroundImg = tk.PhotoImage(file="background.png")
        self.background_label = tk.Label(self.window, image=self.backgroundImg)
        self.background_label.place(x=0, y=0, relwidth=1, relheight=1)
        self.window.resizable(False, False)
        self.buttonFont = font.Font(family="Times", size=16)
        # Create StringVar for future entry boxes
        self.Entry1_text = tk.StringVar()
        self.Entry2_text = tk.StringVar()
        # Create the two main frames
        self.startFrame = tk.Frame(self.window, bg = "#3d786c")
        self.playFrame  = tk.Frame(self.window, bg = "#3d786c")
        # Create empty list for buttons
        self.PlayButtons = list()

    def createPlayers(self, player_name1, player_name2):
        self.player1 = Player.Player(player_name1, "X", True)
        self.player2 = Player.Player(player_name2, "O", False)
        self.player1_Score["text"] = self.player1.user_name + "'s Score: "
        self.player2_Score["text"] = self.player2.user_name + "'s Score: "

    def createImages(self):
        self.myGameIcon = Image.open("icon.png")
        self.myGameIcon = self.myGameIcon.resize((150, 150), Image.ANTIALIAS)
        self.myGameIcon  = ImageTk.PhotoImage(self.myGameIcon)

        self.x = Image.open("X.png")
        self.x = self.x.resize((60, 60), Image.ANTIALIAS)
        self.x = ImageTk.PhotoImage(self.x)

        self.o = Image.open("O.png")
        self.o = self.o.resize((60, 55), Image.ANTIALIAS)
        self.o = ImageTk.PhotoImage(self.o)

        self.emptyImg = Image.open("empty.png")
        self.emptyImg = self.emptyImg.resize((70, 70), Image.ANTIALIAS)
        self.emptyImg = ImageTk.PhotoImage(self.emptyImg)

    def createLabels(self):

        self.picture_heading = tk.Label(image=self.myGameIcon, bg="#3d786c")    
        self.infoLabel       = tk.Label(text="Enter Player Names:", width=25, height=1, 
                                        font="Times 15 bold", bg="#73a09c", fg="white")
        self.X_label         = tk.Label(image=self.x, bg="#3d786c")
        self.O_label         = tk.Label(image=self.o, bg="#3d786c")
        self.heading         = tk.Label(font="Times 20 bold", 
                                        text="My Tic-Tac-Toe Game!", height=2, 
                                        bg="#3d786c", foreground="white")
        self.player1_Score   = tk.Label(font="Times 20 bold", 
                                        height=2, width=16,
                                        bg="#df2a2a", foreground="white")
        self.player2_Score   = tk.Label(font="Times 20 bold", 
                                        height=2, width=16,
                                        bg="#40b8e1", foreground="white")

    def createEntrys(self):
        self.Entry1 = tk.Entry(width = 25, textvariable = self.Entry1_text)
        self.Entry2 = tk.Entry(width = 25, textvariable = self.Entry2_text)
  
    def Start_Command(self):
        self.createPlayers(self.Entry1.get(), self.Entry2.get())
        self.startFrame.pack_forget()
        self.playFrame.pack()
  
    def createButtons(self):
        self.StartButton = tk.Button(text="Start",font="Times 15 bold", width=25, 
                                     height=1, bg="#3df14d", fg="#260273", command=self.Start_Command)

        for i in range(9):
            self.PlayButtons.append(GB.GameButton(image=self.emptyImg, width=70, height=70,
                                                  command=partial(self.playAction, i)))


    def createStartFrame(self):
        # Create special ordered lists of widgets
        Frame1L = [self.heading, self.picture_heading, self.infoLabel]
        Frame2L = [self.X_label, self.Entry1, self.O_label, self.Entry2]
        # Call the Frame builder to build the desired frames
        Frame1 = FB.FrameBuilder.getFrame(self.startFrame, Frame1L, "pack", "#3d786c")
        Frame2 = FB.FrameBuilder.getFrame(self.startFrame, Frame2L, "grid", "#3d786c", 2)
        Frame3 = FB.FrameBuilder.getFrame(self.startFrame,self.StartButton,"pack", "#3d786c")
        # Pack the new Frames in the correct order   
        Frame1.pack()
        Frame2.pack()
        Frame3.pack()
        self.startFrame.pack()

    def createPlayFrame(self):
        # Create the list for the Frame Bulder
        Frame4L = [self.player1_Score, self.player2_Score]
        # Create new frames
        Frame4 = FB.FrameBuilder.getFrame(self.playFrame, Frame4L, "pack", "#3d786c")
        Frame5 = FB.FrameBuilder.getFrame(self.playFrame, self.PlayButtons, "grid", "#3d786c", 3)
        # Pack the new Frames
        Frame4.pack()
        Frame5.pack()

    def playAction(self, i):
        if (self.PlayButtons[i].token == None):
            if (self.player1.turn == True):                  
                self.PlayButtons[i].configure(image=self.x, width=70, height=70)
                self.PlayButtons[i].changeToken(self.player1.token)
                self.player1.turn = False
                #print(self.player1.turn)
            elif (self.player1.turn == False):
                self.PlayButtons[i].configure(image=self.o, width=70, height=70)
                self.PlayButtons[i].changeToken(self.player2.token)
                self.player1.turn = True
                #print(self.player1.turn)
            GL.GameFunctions.MainLogic(self.window, self.PlayButtons, self.emptyImg, self.player1, 
                                       self.player2, self.player1_Score, self.player2_Score)

