# A class responsible for the game logic by providing various 
# functionalities such as :
#   - checking for winner
#   - counting score
#   - anounncing the winner

class GameFunctions:

    global MaxButtonPresses
    MaxButtonPresses = 9

    @staticmethod
    def clearPlayground(buttons,image):
        for i in range(9):
            buttons[i]["image"]=image
            buttons[i].changeToken(None)

    @staticmethod
    def ChangeColor(buttons, index1, index2, index3, color):
        buttons[index1].configure(bg = color)
        buttons[index2].configure(bg = color)
        buttons[index3].configure(bg = color)

    @staticmethod
    def Anounce(root, buttons, index1, index2, index3, color1, color2):
        GameFunctions.ChangeColor(buttons, index1, index2, index3, color1)
        root.after(2000, lambda: GameFunctions.ChangeColor(buttons, index1, index2, index3, color2))

    @staticmethod
    def MainLogic(root, buttons, image, player1, player2, label1, label2):
        
        global MaxButtonPresses
        # Checks horizontal line 1
        if (buttons[0].token == buttons[1].token == buttons[2].token and buttons[0].token != None):

            if (buttons[0].token == player1.token):
                GameFunctions.clearPlayground(buttons, image)
                player1.setScore()
                label1.configure(text=player1.user_name + "'s Score: " + str(player1.score))
                MaxButtonPresses = 9
            if (buttons[0].token == player2.token):
                GameFunctions.clearPlayground(buttons, image)
                player2.setScore()
                label2.configure(text=player2.user_name + "'s Score: " + str(player2.score))   
                MaxButtonPresses = 9
            GameFunctions.Anounce(root, buttons, 0, 1, 2, "#96ff33", "#ffffff")

        # Checks horizontal line 2
        elif (buttons[3].token == buttons[4].token == buttons[5].token and buttons[3].token != None):
        
            if (buttons[3].token == player1.token):
                GameFunctions.clearPlayground(buttons, image)
                player1.setScore()
                
                label1.configure(text=player1.user_name + "'s Score: " + str(player1.score))
                MaxButtonPresses = 9

            if (buttons[3].token == player2.token):
                GameFunctions.clearPlayground(buttons, image)
                player2.setScore()
                
                label2.configure(text=player2.user_name + "'s Score: " + str(player2.score))   
                MaxButtonPresses = 9

            GameFunctions.Anounce(root, buttons, 3, 4, 5, "#96ff33", "#ffffff")
        # Checks horizontal line 3
        elif (buttons[6].token == buttons[7].token == buttons[8].token and buttons[6].token != None):
        
            if (buttons[6].token == player1.token):
                GameFunctions.clearPlayground(buttons, image)
                player1.setScore()
                
                label1.configure(text=player1.user_name + "'s Score: " + str(player1.score))
                MaxButtonPresses = 9
            if (buttons[6].token == player2.token):
                GameFunctions.clearPlayground(buttons, image)
                player2.setScore()
                
                label2.configure(text=player2.user_name + "'s Score: " + str(player2.score))   
                MaxButtonPresses = 9

            GameFunctions.Anounce(root, buttons, 6, 7, 8, "#96ff33", "#ffffff")
        # Checks vertical line 1
        elif (buttons[0].token == buttons[3].token == buttons[6].token and buttons[0].token != None):
        
            if (buttons[0].token == player1.token):
                GameFunctions.clearPlayground(buttons, image)
                player1.setScore()
                
                label1.configure(text=player1.user_name + "'s Score: " + str(player1.score))
                MaxButtonPresses = 9
            if (buttons[0].token == player2.token):
                GameFunctions.clearPlayground(buttons, image)
                player2.setScore()
                
                label2.configure(text=player2.user_name + "'s Score: " + str(player2.score))   
                MaxButtonPresses = 9

            GameFunctions.Anounce(root, buttons, 0, 3, 6, "#96ff33", "#ffffff")
        # Checks vertical line 2
        elif (buttons[1].token == buttons[4].token == buttons[7].token and buttons[1].token != None):
            
            if (buttons[1].token == player1.token):
                GameFunctions.clearPlayground(buttons, image)
                player1.setScore()
                
                label1.configure(text=player1.user_name + "'s Score: " + str(player1.score))
                MaxButtonPresses = 9
            if (buttons[1].token == player2.token):
                GameFunctions.clearPlayground(buttons, image)
                player2.setScore()
                
                label2.configure(text=player2.user_name + "'s Score: " + str(player2.score))
                MaxButtonPresses = 9
            
            GameFunctions.Anounce(root, buttons, 1, 4, 7, "#96ff33", "#ffffff")
        # Checks vertical line 3
        elif (buttons[2].token == buttons[5].token == buttons[8].token and buttons[2].token != None):
        
            if (buttons[2].token == player1.token):
                GameFunctions.clearPlayground(buttons, image)
                player1.setScore()
                
                label1.configure(text=player1.user_name + "'s Score: " + str(player1.score))
                MaxButtonPresses = 9
            if (buttons[2].token == player2.token):
                GameFunctions.clearPlayground(buttons, image)
                player2.setScore()
                
                label2.configure(text=player2.user_name + "'s Score: " + str(player2.score)) 
                MaxButtonPresses = 9

            GameFunctions.Anounce(root, buttons, 2, 5, 8, "#96ff33", "#ffffff")
    # Checks diagonal line 1
        elif (buttons[0].token == buttons[4].token == buttons[8].token and buttons[0].token != None):
            
            if (buttons[0].token == player1.token):
                GameFunctions.clearPlayground(buttons, image)
                player1.setScore()
                
                label1.configure(text=player1.user_name + "'s Score: " + str(player1.score))
                MaxButtonPresses = 9
            if (buttons[0].token == player2.token):
                GameFunctions.clearPlayground(buttons, image)
                player2.setScore()
                
                label2.configure(text=player2.user_name + "'s Score: " + str(player2.score))
                MaxButtonPresses = 9
            GameFunctions.Anounce(root, buttons, 0, 4, 8, "#96ff33", "#ffffff")

    # Checks diagonal line 2
        elif (buttons[2].token == buttons[4].token == buttons[6].token and buttons[2].token != None):
            
            if (buttons[2].token == player1.token):
                GameFunctions.clearPlayground(buttons, image)
                player1.setScore()
                
                label1.configure(text=player1.user_name + "'s Score: " + str(player1.score))
                MaxButtonPresses = 9

            if (buttons[2].token == player2.token):
                GameFunctions.clearPlayground(buttons, image)
                player2.setScore()
                
                label2.configure(text=player2.user_name + "'s Score: " + str(player2.score)) 
                MaxButtonPresses = 9

            GameFunctions.Anounce(root, buttons, 2, 4, 6, "#96ff33", "#ffffff")

    # Checks if all the buttons have been assigned a token
        else:
            MaxButtonPresses -= 1
            if (MaxButtonPresses == 0):
                GameFunctions.clearPlayground(buttons, image)
                MaxButtonPresses = 9


