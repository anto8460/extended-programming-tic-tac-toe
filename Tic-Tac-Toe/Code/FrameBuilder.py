# Class for building different frames for the application:
#  - contains one static method that accepts:
#    * widget_list  - a list of widgets to add to the frame
#    * root         - for the newly created frame;
#    * color        - for the frame
#    * geom_man     - geometry manager
#    * size         - number of rows and columns for the grid manager
#
#  Note: the list of widget should be ordered accordingly in order for the 
#        builder to place the object in the right place

import tkinter as tk
from time import sleep

class FrameBuilder:
    @staticmethod
    def getFrame(root, widget_list, geom_man, color="#FFFFFF", size=0,):       
        frame = tk.Frame(root, bg=color)
        if geom_man == "pack" and type(widget_list) == list:
            for obj in widget_list:
                obj.pack(in_ = frame)            
        elif geom_man == "grid" and type(widget_list) == list:
            index = 0
            for i in range(1, size+1):
                for k in range(1, size+1):
                    widget_list[index].grid(row=i, column=k, padx=5, pady=5, in_=frame)
                    index += 1
            del index
        else:
            widget_list.pack(in_= frame)

        return frame




