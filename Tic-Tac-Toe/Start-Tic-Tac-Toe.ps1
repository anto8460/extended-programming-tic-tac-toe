$t = '[DllImport("user32.dll")] public static extern bool ShowWindow(int handle, int state);'
add-type -name win -member $t -namespace native
[native.win]::ShowWindow(([System.Diagnostics.Process]::GetCurrentProcess() | Get-Process).MainWindowHandle, 0)
cd Code
# Starting the virtual environment
.\env\Scripts\activate
# Installing module Pillow
py -m pip install Pillow
# Starting the game
py .\main.py
# Deactivating venv
deactivate